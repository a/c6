# c6

Discord bot to nuke accidentally posted secrets.

The name comes from the fact that it can nuke yubico OTPs, where pre-built ones start with cccccc.

---

### Supported values

- Yubico OTP (sent to YubiCloud to block that OTP code)
~~- 7b NXP 14a UIDs, which are likely [NExT](https://dangerousthings.com/product/next/) UIDs accidentally posted using a [KBR1](https://dangerousthings.com/product/kbr1/)~~ (this has been disabled due to false positives)

---

### Invite

https://discordapp.com/api/oauth2/authorize?client_id=804004389772984351&permissions=8192&scope=bot

---

### Credits-ish

*Is it really credits if it's all me?*

- Code is based on the now-rather-old-and-ugly [dpybotbase](https://gitlab.com/a/dpyBotBase) by yours truly.
- Yubico OTP code is based on the [yubicootp cog for robocop-ng](https://github.com/reswitched/robocop-ng). Relevant credit from there: [Thanks to] linuxgemini for helping out with Yubico OTP revocation code (which is based on their work)
