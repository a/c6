from discord.ext.commands import Cog
import re
import asyncio


class NextUID(Cog):
    def __init__(self, bot):
        self.bot = bot
        self.uid_re = re.compile(r"(04[0-9a-fA-F]{12})$")
        self.twitter_re = re.compile(
            r"twitter\.com/[^\/]+/status/[0-9]+(04[0-9a-fA-F]{12})$"
        )

    @Cog.listener()
    async def on_message(self, message):
        await self.bot.wait_until_ready()

        # Note: This only finds one entry at the end of post
        # due to the way the regex is written.
        uids = self.uid_re.findall(message.content.strip())
        twitter_links = self.twitter_re.findall(message.content.strip())
        if uids and not twitter_links:
            await message.delete()

            # Inform user
            msg = await message.channel.send(
                f"{message.author.mention}: Ate the UID you posted as it looks accidental. "
                "You can finish off your post with anything but an UID to make me ignore it (like a dot). "
                "This message will self destruct in 5 seconds."
            )

            # and delete our message after 5s to help SNR
            await asyncio.sleep(5)
            await msg.delete()


def setup(bot):
    bot.add_cog(NextUID(bot))
